
define(['app/linefact', 'app/eventbus', 'app/eventproxy', 'app/fontcalc'],
    function( linefact, eventbus, eventproxy, fontcalc ) {
	
		var Editor = {
			/*
				This class is the builder of the editor
			*/
		
			make: function(containerClass) {
				var canvas = $(containerClass);

				//mark canvas
				canvas.addClass('ideCanvas');

				var inputTX = document.createElement('textarea');
				inputTX.className = 'inputTX';

				//add event handler
				inputTX.onkeydown = function(event)	{
					eventbus.publish("elem", 'keydown', this, event);
				};

				canvas[0].appendChild( inputTX );

				var textRenderingSection = document.createElement('div');
				textRenderingSection.className = 'textRenderingSection';

				canvas[0].onclick = function(event) {
					eventbus.publish("elem", 'click', this, event);
					eventbus.publish("internalSharedVars", "currentLine", null, null).set(this);
				};

				canvas[0].onkeyup = function(event) {
					eventbus.publish("elem", 'keyup', this, event);
					eventbus.publish("internalSharedVars", "currentLine", null, null).set(this);
				};

				canvas[0].onmouseup = function(event) {
					eventbus.publish("elem", 'mouseup', this, event);
				};

				canvas[0].onmousemove = function(event) {
					eventbus.publish("elem", 'mousemove', this, event);
				};

				canvas[0].onmousedown = function(event) {
					eventbus.publish("elem", 'mousedown', this, event);
				};

				eventbus.subscribeVar('textRenderingSection', textRenderingSection);

				eventbus.publish("app", 'linefactory', null, [ this, textRenderingSection, null, '' ] );

				//after creation of first line; run font computations

				eventbus.subscribe("app", "getFontMetrics", null, false, function(event, data) {
					var cached = false;
					var cache;

					if( cached == false ) {
						cached = true;
						cache = (new fontcalc( data ));
						return cache;
					} else {
						return cache;
					}
				});

				canvas[0].appendChild( textRenderingSection );

				var textSelectionSection = document.createElement('div');
				textSelectionSection.className = 'textSelectionSection';

				eventbus.subscribeVar('textSelectionSection', textSelectionSection);

				canvas[0].appendChild( textSelectionSection );

				//start decoupled event handling system
				(new eventproxy());

				return canvas;
			},


			makeWithData: function(containerClass, data) {
				//this needs to be better
				var textRenderer = this.make( containerClass ).children()[1];

				//clear everything //this should be better and use the blank line added in the native make
				textRenderer.innerHTML = '';

				for(var x=0; x<=data.length-1; x++) {
		
					eventbus.publish("app", 'linefactory', null, [ this, textRenderer, x, data[x]] );
					//$('#line-renderer-' + x)[0].innerHTML = data[x];
					eventbus.publish("app", 'codeanalyzer', null, null );
				}
				
				//$('#text-render').children().first().focus();
				//this.refreshLines();
		
			}

		};
 
        return Editor;
});



