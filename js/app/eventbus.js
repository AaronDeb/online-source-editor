define([], 
    function() {

        var EventBus = function() {
            
            var subscribe = function(topicName, eventName, object, useMultipleInstances, handler) {
                //define the topic name if undefined
                if( typeof( topics[topicName] ) == "undefined" ) {
                    topics[topicName] = {};
                }
                
                //define the event name if undefined
                if( typeof( topics[topicName][eventName] ) == "undefined" ) {
                    topics[topicName][eventName]=[]; 
                }

                //this was needed cause adding objects sporadically throughout execution was a resource sucker
                if ( useMultipleInstances ) {
                    topics[topicName][eventName].push([object, handler]);

                } else {
                    //if exists replace it
                    if ( topics[topicName][eventName].length > 0 ) {
                        topics[topicName][eventName][0] = [object, handler];
                    } else {
                        topics[topicName][eventName].push([object, handler]);
                    }
                }


            };

            this.hookEvent = function(topicName, eventName, hookType, callback ) {
                //define the event name if undefined
                if( typeof( hooks[topicName] ) == "undefined" ) {
                    hooks[topicName]=[];
                }

                if( typeof( hooks[topicName][eventName] ) == "undefined" ) {
                    hooks[topicName][eventName]=[];
                }

                hooks[topicName][eventName] = [hookType, callback];
            };
        
            var dispatch = function(topicName, eventName, target, data){
                if(topics[topicName][eventName]){
                    for(var i = 0; i < topics[topicName][eventName].length; i++){
                        if( topics[topicName][eventName][i][0] == target ) {

                            //check for pre hooks
                            if( hooks[topicName] != undefined ) {

                                if( hooks[topicName][eventName] ) {

                                    for(var p = 0; p < hooks[topicName][eventName].length; p++){
                                        if( hooks[topicName][eventName][0] == PRE ) { // check if hook type is pre-execution
                                            data = hooks[topicName][eventName][1](data); //can modify input data
                                        }
                                    }

                                }
                            }

                            if( DEBUG ) {
                                if( eventName == 'isTextSelected')
                                    console.log("Event: " + eventName + " was called for topic " + topicName + " at " + Math.round(new Date().getTime()/1000) + "!");
                            }
                            
                            var handlerPair = topics[topicName][eventName][i];
                            var func = handlerPair[1](handlerPair[0], data);

                            //check for post hooks
                            if( hooks[topicName] != undefined ) {
                                if (hooks[topicName][eventName]) {

                                    for (var o = 0; o < hooks[topicName][eventName].length; o++) {
                                        if (hooks[topicName][eventName][0] == POST) { // check if hook type is post-execution
                                            hooks[topicName][eventName][1]( func ); //can modify return data
                                        }
                                    }

                                }
                            }

                            return func;

                        }
                    }
                }

                if( DEBUG ) {
                    console.log("Couldn't find specified event! eventName:" + eventName + " in topic: " + topicName);
                }

            };
            
            var topics = {};
            var hooks = {};
            var PRE = 0,
                POST = 1;
            var DEBUG = true;
            
            this.subscribe = function(topicName, eventName, object, useMultipleInstances, handler) {
                subscribe(topicName, eventName, object, useMultipleInstances, handler);
            };

            //added shared variable support for the eventbus
            this.subscribeVar = function(varName, varContents){

                var handler = function(data) {
                    this.set( data );
                };

                handler.prototype.set = function( varContents ) {
                    var worker = this.worker;
                    if (worker != undefined) {
                        this.content = worker(this.content);
                    } else {
                        this.content = varContents;
                    }
                };

                handler.prototype.setWorker = function ( func ) {
                    this.worker = func; // this is here to enable doing work on a variable
                    this.set( this.content );
                };

                handler.prototype.get = function() {
                    var worker = this.worker;
                    if (worker != undefined) {
                        return worker(this.content);
                    } else {
                        return this.content;
                    }
                };

                var handlerInstance = new handler(varContents);

                subscribe('internalSharedVars', varName, null, false, function() { return handlerInstance; });
            };
            
            this.publish = function(topicName, eventName, target, data){
                return dispatch(topicName, eventName, target, data); //starting to change names, for readability
            };
            
        };
        
        return new EventBus;
        
    }
);
 
