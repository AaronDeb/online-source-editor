define([ 'app/eventbus', 'app/caretext' ],
    function ( eventbus, caretext ) {

        var LineHandler = function(elem) {

            if( elem == undefined ) {
                return;
            }

            elem.parseMe = function() {
                var textContent = this.textContent;
                var fontMetrics = eventbus.publish("app", 'getFontMetrics', null, null );

                var textPixelWidth =0;
                var lineData = [];

                for(var w = 0; w<=textContent.length-1; w++) {
                    textPixelWidth = fontMetrics[ textContent[w].charCodeAt(0) ];
                    lineData.push( textPixelWidth );
                }

                return lineData
            };
            
            elem.insert = function( range, content ) {

                var lineText = this.textContent;

                var cut = function( text, start, end ) {
                    return text.substring(start, end);
                };

                var startRange = range[0];
                var endRange = range[1];

                if( startRange == endRange ) {
                    endRange = lineText.length;
                }

                // insert char at designated position
                this.innerHTML = cut( lineText, 0, startRange ) + content + cut( lineText, startRange, endRange );

                var textPixelWidth =0;
                var parsedLine = this.parseMe();

                for(var w = 0; w<=startRange; w++) {
                    textPixelWidth += parsedLine[ w ];
                }

                if( startRange == 0 && endRange == 0 && content.length ==0 ) {
                    textPixelWidth =0;
                }

                var caret = (new caretext());
                caret.moveX( this, textPixelWidth );
                delete caret;

                eventbus.publish("app", 'codeanalyzer', null, this );

            };

            elem.delete = function( range  ) {
                var lineText = this.textContent;

                var textPixelWidth =0;
                var parsedLine = this.parseMe();

                var cut = function( text, start, end ) {
                    return text.substring(start, end);
                };

                var startRange = range[0];
                var endRange = range[1];

                if( startRange == endRange ) {
                    endRange = lineText.length;
                    // delete char at designated position
                    this.innerHTML = cut( lineText, 0, startRange ) + cut( lineText, startRange+1, endRange );
                } else {

                    // delete char range at designated position
                    this.innerHTML = cut( lineText, 0, startRange ) + cut( lineText, endRange, lineText.length );
                }


                for(var w = 0; w<=startRange-1; w++) {
                    textPixelWidth += parsedLine[ w ];
                }

                var caret = (new caretext());
                caret.moveX( this, textPixelWidth );
                delete caret;

                eventbus.publish("app", 'codeanalyzer', null, this );
            };

            elem.getTextPixelWidth = function( range ) {
                var parsedLine = this.parseMe(); // test
                var textPixelWidth = 0;

                var startRange = range[0];
                var endRange = range[1];

                if( endRange > parsedLine.length || endRange == -1) {
                    //throw ("Out of range!");
                    endRange = parsedLine.length-1;
                }

                if( startRange == -1 ) {
                    startRange = parsedLine.length-1;
                }

                for(var w = startRange; w<endRange; w++) {
                    textPixelWidth += parsedLine[w];
                }

                return textPixelWidth;

            };

            elem.getMousePosition = function( xPos ) {

                if( ! xPos ) {
                    var caret = (new caretext());
                    xPos = caret.getInstance()[0].offsetLeft;
                }

                var lineData = this.parseMe(); //test

                //var init
                var actualCounter = 0;
                var wordLength = 0;
                var textPixelWidth = 0;

                for(var w = 0; w<=lineData.length-1; w++) {
                    actualCounter++;
                    textPixelWidth += lineData[w];
                    if( parseInt(textPixelWidth) <= xPos ) { //was Math.round
                        wordLength = actualCounter;
                        //break;
                    }
                }

                return wordLength;
            };

            elem.getIndex = function() {
                return $('.liner').index( this );
            };

            elem.getClickPosition = function() {
                var jElemName = $( this );
                return {
                    'X': this.offsetLeft,
                    'Y': this.offsetTop,

                    'xMinusBoxSpacing': jElemName.offset().left,
                    'yMinusBoxSpacing': jElemName.offset().top,

                    'width': this.offsetWidth,
                    'height': this.offsetHeight
                }
            };

            elem.moveCursorToWordPosition = function( wordPos ) { //to x position
                var txtCursor = (new caretext());

                var clickMetrics = this.getClickPosition();
                var lineData = this.parseMe(); //test

                var wordsToPixels = 0;

                //check if wordpos exceeds current line length
                if(wordPos >= lineData.length-1) {
                    //reset to acceptable length
                    wordPos = lineData.length-1;
                }

                for(var w = 0; w<=wordPos; w++) {
                    wordsToPixels += lineData[w];
                }

                txtCursor.move(wordsToPixels, clickMetrics.Y);
            };

            elem.moveCursorToMousePosition = function() { //clicked position

                var txtCursor = (new caretext());

                var mouseX = event.pageX;
                var mouseY = event.pageY;

                var clickMetrics = this.getClickPosition();

                var textPixelLength = this.getTextPixelWidth( [0, this.textContent.length] ) ;
                var relativeMouseX = mouseX - clickMetrics.xMinusBoxSpacing;

                var xPosToTextPos = this.getMousePosition( relativeMouseX );
                var textPixelLengthFromZeroToMouse = this.getTextPixelWidth( [0, xPosToTextPos] );

                txtCursor.move(textPixelLengthFromZeroToMouse, clickMetrics.Y);

            };

        };

        return LineHandler;

    }
);
 
