
define([ 'app/util', 'app/eventbus', 'app/syntaxhighlighter', 'app/codecompletor' ], 
    function( util, eventbus ) {

		//Strict patterns
		var detectPattern = [
			{
				toDetect: "function", 
				regPattern: "(public|private|internal|protected).*?(static|abstract|virtual|unsafe|override).*?(returnVal).*?((?:[a-z][a-z]+))", 
				valueToReplace: "returnVal"
			}
		];

		var classReturns = "string|integer|float|decimal|double";
		
		var CodeAnalyzer = function(div) {
			util.removeCodeCompletor();
			analyse(div);
		
			function analyse(div) {
				if( div == undefined ) {
					return;
				}
				//get ranges				
				var savedRang; //= util.getSelection( div );
				var text = div.textContent;

				//this.processedText = text;
				var matchs = initCodeParser(text);
				for(var x=0; x<=matchs.length-1; x++) {
					findAndLog(text, matchs[x]['patsFound'], matchs[x]['detected']);		
				}
						
				eventbus.publish("app", 'syntaxhighlighter', null, [text, div] ); //SyntaxHighlighter(text);
				//util.restoreSelection(div, savedRang);
				
				//addSpaces(div);
			};
		
			function findAndLog(text, match, toDetect) {
				f = function(mt) {
					if(mt != null) {
						var pt = "";
		
						for(var m=4; m<=mt.length-1 ;m++) {
							pt += mt[m];
							if(m < mt.length-1) {
								pt += "\xa0";
							}
						}
						this.processedText = text.replace(mt[0], pt);
					}
				};
		
				switch(toDetect) 
				{
					case "function":
						f(match);
						break;
				};
		
			};


			function initCodeParser(text) {
				var results = [];
				for(var d=0; d<= detectPattern.length-1; d++) {
					var pat = detectPattern[d]['regPattern'];
					pat = pat.replace("returnVal", classReturns);
					var regObj = new RegExp(pat, "i");
					var matches = regObj.exec(text); //text.match(regObj);
		
					if(matches != null) {
						results.push( { detected: detectPattern[d]['toDetect'] , patsFound: matches } );
					}
				}
				return results;
			}
		
			function getHTMLPosFromTextPos(elem, actPos) {
				var cNodes = elem.childNodes;
				var lenArray = [];
		
				for(var ns= 0; ns <= cNodes.length-1; ns++) {
				    lenArray.push(cNodes[ns].textContent.length);
				}
		
				var totalLen = 0;
				var foundELEM;
				for(var l= 0; l <= lenArray.length-1; l++) {
					if(foundELEM == undefined) {
				    	totalLen += lenArray[l];
					    for(var n=0; n<= totalLen; n++) {
						    if(actPos == n){
						        foundELEM = l;
						        break;
						    }
						}
					}
				}
		
				//resize actual pos
				for(var u =0; u<=foundELEM-1; u++) {
					actPos = actPos - lenArray[u];
				}
		
				var elemHTML = cNodes[foundELEM];
				var elemText = '';
				if(elemHTML.nodeName != '#text') {
				    elemText = cNodes[foundELEM].innerHTML;
				} else {
					elemText = cNodes[foundELEM].data;
				}
		
				var strUNTIL = '';
				for(var w =0; w<=actPos-1; w++) {
				    strUNTIL += elemText[w];
				}
		
				var CURRENT_NODE_POS =0;
				if(elemHTML.nodeName != '#text') {
					for(var w=0; w<= elemHTML.outerHTML.length-1; w++) {
						if(elemHTML.outerHTML[w] == '>') {
							CURRENT_NODE_POS++;
							CURRENT_NODE_POS += strUNTIL.length-1;
							break;
						}
						CURRENT_NODE_POS++;
					}
				} else {
					CURRENT_NODE_POS = strUNTIL.length;
				}
				
				for(var y =0; y<=foundELEM-1; y++) {
					if(cNodes[y].nodeName != '#text') {
						CURRENT_NODE_POS += cNodes[y].outerHTML.length-1;
					} else {
						CURRENT_NODE_POS += cNodes[y].data.length-1;
					}
				}
		
				return CURRENT_NODE_POS;
			};
		
		};
		
		eventbus.subscribe("app", "codeanalyzer", null, true, function(event, data) {
			var line = eventbus.publish("internalSharedVars", 'currentLine', null, null).get();
			if( data == null) {
				( new CodeAnalyzer(line) );
			} else {
				( new CodeAnalyzer(data) );
			}
		});
			
		//return CodeAnalyzer;
    }
);