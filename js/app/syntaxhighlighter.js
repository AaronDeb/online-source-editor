define(['underscore', 'app/eventbus'], 
    function( _ , eventbus) {
    	
    	//var cmvReturns = [ "string", "integer", "float", "decimal", "double", "public" ];

		var regPatts = {
			'operator': /--?|\+\+?|!=?=?|<=?|>=?|==?=?|&&?|\|\|?|\?|\*|\/|~|\^|%/g,
			'punctuation': /[{}[\];(),.:]/g,
    		'keyword': /\b(abstract|as|async|await|base|bool|break|byte|case|catch|char|checked|class|const|continue|decimal|default|delegate|do|double|else|enum|event|explicit|extern|false|finally|fixed|float|for|foreach|goto|if|implicit|in|int|interface|internal|is|lock|long|namespace|new|null|object|operator|out|override|params|private|protected|public|readonly|ref|return|sbyte|sealed|short|sizeof|stackalloc|static|string|struct|switch|this|throw|true|try|typeof|uint|ulong|unchecked|unsafe|ushort|using|virtual|void|volatile|while|add|alias|ascending|async|await|descending|dynamic|from|get|global|group|into|join|let|orderby|partial|remove|select|set|value|var|where|yield)\b/g,
			'boolean': /\b(true|false)\b/g,
			'function': /[a-z0-9_]+(?=\()/i,
			'number': /\b-?(?:0x[\da-f]+|\d*\.?\d+(?:e[+-]?\d+)?)\b/i,				
    	};

    	
		var SyntaxHighlighter = function(text) {
		
			return initSyntaxHighligher(text);
		
			function initSyntaxHighligher(text) {
				var arrKeys = Object.keys(regPatts);
				var arrSize = _.size(regPatts);				
		
				var wordsFound = text.split(String.fromCharCode(32));
				var wordArr = [];
				
				var editedText = text;

				//need to check what this does (looks complicated)
				for(var w=0; w<=wordsFound.length-1; w++) {
					var f = wordsFound[w].split(String.fromCharCode(160));
					if(f.length <= 1) {
						wordArr.push( { 'value': f[0], 'edited': false } );
					} else {
						for(var t =0; t<= f.length-1; t++) {
							wordArr.push( { 'value': f[t], 'edited': false } );
						}
					}
				}
		
				//replacement loop, where words from the line div are matched with the regex matches and replaced in the array
				wordArr = _.uniq( wordArr, function(a){
					return a['value'];
				});
				for(var w=0; w<=wordArr.length-1; w++) { // for each word in the sentence

					for(var p=0; p<=arrSize-1; p++) { // for each regex pattern group
					
						var regResults = text.match( regPatts[ arrKeys[p] ] );

						if( regResults != null ) { // if no matches found stop
							
							for(var m=0; m<=regResults.length-1; m++) { //for each matched word

								if(wordArr[w]['value'].trim() == regResults[m] && wordArr[w]['edited'] == false) {
									wordArr[w]['value'] = replaceAll(wordArr[w]['value'], wordArr[w]['value'], "<span class='" + arrKeys[p] + "' style='color: #569CD6;'>" + wordArr[w]['value'] + "</span>");
									wordArr[w]['edited'] = true;
								}

							}

						}
					}		
					
				}

				var concatArr = wordArr.map(function(a){
					return a['value'];
				}).join('&nbsp;');

				return concatArr;
			}
		
			function replaceAll(str, find, replace) {
				//return str.replace(find, replace);
		   		return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
			}
		
		};
		
		eventbus.subscribe("app", "syntaxhighlighter", null, true, function(event, data) {
			//console.log("init sh");
			$(data[1]).html( SyntaxHighlighter(data[0]) );
		});
		
		//return SyntaxHighlighter;
	}
);

