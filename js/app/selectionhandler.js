define(['app/lineext', 'app/selection', 'app/eventbus'],
    function( lineext, selection, eventbus ) {

        var SelectionHandler = function( ) {

            this.delete = function( selection ) {
                var txtSelectionSection = eventbus.publish("internalSharedVars", "textSelectionSection", null, null).get();
                txtSelectionSection.innerHTML = "";

                for (var i =0; i< selection.lineArray.length; i++) {
                    var lineIndex = selection.lineArray[i]["lineIndex"];
                    var line = getLineByIndex(lineIndex);

                    var startPos = selection.lineArray[i]["startPos"];
                    var endPos = selection.lineArray[i]["endPos"];

                    if( endPos != 999999 ) {
                        if (startPos > endPos) {
                            line.delete([endPos, startPos]);
                        } else {
                            line.delete([startPos, endPos]);
                        }
                    } else {

                        if( i==0 && selection.direction == 1 ){ //if direction upwards
                            pixelWidth = line.getTextPixelWidth([ -1, currentPos ]) + 'px';
                        } else if ( i==selection.lineArray.length-1 && selection.direction == 1 ) {
                            pixelWidth = '100%'
                        }
                    }

                }
            };

            this.insert = function( selection ) {

                var txtSelectionSection = eventbus.publish("internalSharedVars", "textSelectionSection", null, null).get();
                txtSelectionSection.innerHTML = "";

                for (var i =0; i< selection.lineArray.length; i++) {
                    var lineIndex = selection.lineArray[i].lineIndex;
                    var line = getLineByIndex(lineIndex);

                    var currentPos = line.getMousePosition();

                    var startPos = selection.lineArray[i].startPos;
                    var endPos = selection.lineArray[i].endPos;

                    var selectionLine = document.createElement('div');
                    selectionLine.className = 'selectionRenderer';
                    selectionLine.style.top =  lineIndex*line.getClickPosition().height + 'px';
                    selectionLine.style.height = line.getClickPosition().height + 'px';

                    var leftPixelPos = line.getTextPixelWidth([0, startPos]);
                    var pixelWidth;


                    if( endPos != 9999999 ) { //if endpos is 9999999 [impossible] then make width 100%
                        pixelWidth = line.getTextPixelWidth([startPos, endPos]) + 'px';
                    } else {
                        pixelWidth = '100%';
                    }

                    //console.log(i , selection.direction);
                    if( i==0 && selection.direction == 1 ){ //if direction upwards
                        pixelWidth = line.getTextPixelWidth([ -1, currentPos ]) + 'px';
                    } else if ( i==selection.lineArray.length-1 && selection.direction == 1 ) {
                        pixelWidth = '100%'
                    }

                    selectionLine.style.left = leftPixelPos + 'px';
                    selectionLine.style.width = pixelWidth;

                    if( startPos > endPos ) {
                        pixelWidth = line.getTextPixelWidth([ endPos, startPos ]);
                        selectionLine.style.left = leftPixelPos-pixelWidth + 'px';
                        selectionLine.style.width = pixelWidth + 'px';
                    }

                    txtSelectionSection.appendChild(selectionLine);
                }

            };

            var getLineByIndex = function( index ) {
                return $('.liner')[index];
            };

        };

        return SelectionHandler;

    }
);

