
define([ 'app/eventbus', 'app/lineext', 'app/caretext', 'app/codeanalys'],
	function (eventbus, lineext, caretext) {

		function LineFactory() {

			var bindEvent = function(element, type, handler) {
				if(element.addEventListener) {
					element.addEventListener(type, handler, false);
				} else {
					element.attachEvent('on'+type, handler);
				}
			};

			eventbus.subscribeVar("currentLine", this.currentLine);

			eventbus.publish("internalSharedVars", 'currentLine', null, null).setWorker( function( data ) {
				/*if( data != undefined ) {
					var me = this.content;
					var lineText = me.textContent;

					var fontMetrics = eventbus.publish("app", 'getFontMetrics', null, me );
					var textPixelWidth =0;

					var actualCounter = 0;
					var line = [];
					for(var w = 0; w<=lineText.length-1; w++) {
						actualCounter++;
						var char = lineText[w].charCodeAt(0);
						var charPixelWidth = fontMetrics[ lineText[w].charCodeAt(0) ];
						line.push( [char, charPixelWidth ] );
					}

					return [ data, line ];
					// [currentLine, parsedVersion];
				} else {
					return data;
				}*/

				//this should return the line clicked from the text-rendering section click event

				var canvas = $('.textRenderingSection')[0];

				if( ! canvas ){
					return;
				}

				var lines = canvas.childNodes;

				if( event ) {
					if ( event.pageX == null ) {

						var caret = (new caretext());
						var mouseY = caret.getInstance().offset().top;
						var mouseX = caret.getInstance().offset().left;

					} else {

						var mouseX = event.pageX;
						var mouseY = event.pageY;
					}
				}

				//this is here to smooth out click position because of the text cursor height
				if( mouseY >= 7)
					mouseY -= 7;


				for(var l=0; l<=lines.length-1; l++) {
					var clickMetrics = lines[l].getClickPosition();

					var heightMultiplier = 1;
					if (l != 0)
						heightMultiplier = l - 1;


					if( mouseY >= clickMetrics.Y && mouseY <= clickMetrics.Y+clickMetrics.height ) {
						return lines[l];
					}

				}

				return null;

			});

			var makeLine = function()	{
				//CREATE NEW LINE RENDER
				var newLineRend = document.createElement('div');
				//newLineRend.id = 'line-renderer-' + lineNo;
				newLineRend.className = 'liner';

				eventbus.publish("internalSharedVars", "currentLine", null, null).set(newLineRend);

				newLineRend.onfocus = function(event) {
					eventbus.publish("elem", 'focus', this, event);
					eventbus.publish("internalSharedVars", "currentLine", null, null).set(this);
				};

				newLineRend.onblur = function(event) {
					eventbus.publish("elem", 'blur', this, event);
					eventbus.publish("internalSharedVars", "currentLine", null, null).set(this);
				};

				lineext( newLineRend );

				return newLineRend;
			};

			this.makeLine = function( ) {
				return makeLine();
			};

		};


		eventbus.subscribe("app", "linefactory", null, true, function(event, data) {

			var linFactory = new LineFactory(data[0]);
			var line = linFactory.makeLine();

			line.innerText = data[3];

			if( data[2] == null ) {
				//append it wherever
				data[1].appendChild( line );
			} else {

				if( data[2] == 0 ) {
					var canvas = $('.ideCanvas .textRenderingSection'); //get the child right before the one needed
					canvas.prepend( line );
				} else {
					var rightBeforeChild = $(data[1]).children()[data[2] - 1]; //get the child right before the one needed
					$( line ).insertAfter(rightBeforeChild);
				}
			}

			return line;
		});

		//return LineFactory;
	}
);

