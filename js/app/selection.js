define(['app/lineext'],
    function( lineext ) {

        var init = function( data ) {

            var downWards=  -1;
            var upWards= 1;
            var sameLine=  0;

            var direction=  0;
            var lineArray=  [];

            var startLine = data["startIndex"];
            var endLine = data["endIndex"];

            if(startLine == endLine) {
                direction = sameLine;
                lineArray.push(
                    { 'lineIndex': startLine, 'startPos': data["startPos"],'endPos': data["endPos"] }
                );

            } else if(startLine < endLine) {

                direction = downWards;

                lineArray.push(
                    { 'lineIndex': startLine, 'startPos': data["startPos"],'endPos': 9999999 }
                );


                var elementIndexDifference = (endLine -startLine);


                if( elementIndexDifference >= 1 ) {

                    for(var i =startLine+1; i<= endLine-1; i++) {

                        lineArray.push(
                            { 'lineIndex': i, 'startPos': 0,'endPos': 9999999 }
                        );

                    }

                }

                lineArray.push(
                    { 'lineIndex': endLine, 'startPos': 0,'endPos': data["endPos"] }
                );

            } else {
                direction = upWards;
                lineArray.push(
                    { 'lineIndex': startLine, 'startPos': 0,'endPos': data["startPos"] }
                );


                var elementIndexDifference = (startLine - endLine );
                if( elementIndexDifference >= 1 ) {

                    for(var i =endLine+1; i<= elementIndexDifference-1; i++) {

                        lineArray.push(
                            { 'lineIndex': i, 'startPos': 0,'endPos': 9999999 }
                        );

                    }

                }

                lineArray.push(
                    { 'lineIndex': endLine, 'startPos': data["endPos"],'endPos': -1 }
                );

            }

            return { "lineArray": lineArray, "direction": direction };

        };

        return init;

    }
);

