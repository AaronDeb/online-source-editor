/**
 * Created by Aaron on 1/5/2016.
 *
 * THIS SHOULD BE CALCULATED ONCE AND THEN CACHED
 */


define([],
    function () {

        function FontCalculator() {

            var getStyle = function(el, styleProp) {
                var camelize = function (str) {
                    return str.replace(/\-(\w)/g, function(str, letter){
                        return letter.toUpperCase();
                    });
                };

                if (el.currentStyle) {
                    return el.currentStyle[camelize(styleProp)];
                } else if (document.defaultView && document.defaultView.getComputedStyle) {
                    return document.defaultView.getComputedStyle(el,null)
                        .getPropertyValue(styleProp);
                } else {
                    return el.style[camelize(styleProp)];
                }
            };

            /*var fontSize = getStyle( elem, 'font-size');
            var fontFamily = getStyle( elem, 'font-family'); */ // this needs to be though better
            var alphaNumericCharsWithLength = [];

            var fontTestDiv = document.createElement('div');
            fontTestDiv.className = 'fontTestDiv';

            $('body')[0].appendChild(fontTestDiv);

            var testDiv = $('.fontTestDiv');

            //special char handler //for space specifically
            testDiv[0].innerHTML = '&nbsp;';
            var length = testDiv[0].getBoundingClientRect().width;
            alphaNumericCharsWithLength[32] = length;

            // loop for chars
            for (var i = 33; i <= 160; i++) {
                testDiv[0].innerText = String.fromCharCode(i);
                length = testDiv[0].getBoundingClientRect().width;
                alphaNumericCharsWithLength[i] = length;
            }

            //destroy object
            testDiv.remove();

            return alphaNumericCharsWithLength;

        };


        return FontCalculator;
    }
);

