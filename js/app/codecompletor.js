define([ 'app/util', 'app/eventbus' ], 
    function( util, eventbus ) {
    	
		var cmvReturns = [ "string", "integer", "float", "decimal", "double", "public" ];
		
    	var CodeCompletor =  function(ev, elem) {

			init(ev, elem);
			
			function init(ev, elem) {
				//removeNoisySpaces(elem);
				
				var savedRang; //= util.getSelection( elem );
		
				var text = elem.textContent;
				var l = 0;
		
				var wordPiece = '';
		
		
				for(var w = (savedRang.start-1); w >= l; w--) {
					if(text[w].charCodeAt(0) == 160 || text[w].charCodeAt(0) == 46 || text[w].charCodeAt(0) == 32) {
						l = w;
						break;
					} else {
						wordPiece += text[w];
					}
				}
		
				var str = invertString(wordPiece);
				var wordMatchs = findWordsStartingWith(str);
		
				if(wordMatchs.length > 0) {
		
					var body = $('#file-drag')[0];
					var sens = fabricateIntellisensor(wordMatchs);
					sens.style['top'] = elem.offsetTop + 20 + 'px';
					sens.style['left'] = elem.offsetLeft + savedRang.start * 5 + 'px';
		
					body.appendChild(sens);
		
					
				}

				///util.restoreSelection(elem, savedRang);
		
				//addSpaces(elem);
			}
		
			function fabricateIntellisensor(wrds) {
				/*
				<ul class='dropdownmenu'>
					<li>
						<a tabindex="-1" href="#">word</a>
					</li>
				</ul>
				*/
		
				wrds.sort();
		
				var ulContainer = document.createElement('ul');
				ulContainer.className = 'dropdownmenu';
		
				for(var w =0; w <= wrds.length-1; w++) {
					var liWord = document.createElement('li');
					var a = document.createElement('a');
		
					
					a.innerHTML = boldOutSelection(wrds[w]['word'], 0, wrds[w]['wordsMatched']);
					liWord.appendChild(a);
		
					if(w==0) {
						liWord['active'] = 'true';
						liWord.style['background-color'] = 'rgba(201, 201, 201, 0.28)';
					} else {
						liWord['active'] = 'false';
						liWord.style['background-color'] = '#FFF';
					}
		
					liWord.id = w;
					
					liWord.onclick = function() { 
						var currID = this.id;
						var pluckedWord = {word: wrds[currID]['word'], wordsMatched: wrds[currID]['wordsMatched']};
						var wrd = pluckedWord['word'];
						var mwrdl = pluckedWord['wordsMatched'];
						insertThisWord(wrd, mwrdl); 
					};
		
					ulContainer.appendChild(liWord);
				}
		
				ulContainer.contentEditable = false;
				return ulContainer;
			}
		
			function insertThisWord(word, lenToStartFrom) {

				var currDiv = eventbus.publish("internalSharedVars", 'currentLine', null, null).get(); //$('#line-renderer-' + currentLine)[0];
				var s; //= util.getSelection(currDiv);
				var endPosition = s.start; //current start position
		
				var continuedWord = '';
				for(var c=lenToStartFrom; c<=word.length-1; c++) {
					continuedWord += word[c];
					endPosition++; //update the end position with the words added
				}
		
				var txt = currDiv.textContent;
				
				//edit the current text and add the needed words
				currDiv.textContent = txt.substring(0, s.start) + continuedWord + txt.substring(s.start, txt.length);
		
				//bubble up that event
				currDiv.dispatchEvent(new Event('contentChange'));
		
				//restore selection to the new position
				//util.restoreSelection( currDiv, {start: endPosition, end: endPosition} );
		
				//Finally remove the completor
				util.removeCodeCompletor();
			}
		
			function boldOutSelection(wrd, start, end) {
				var bldWrd = '';
				for(var w = 0; w<=wrd.length-1; w++) {
					if(w == start) {
						bldWrd += '<b>';
					} else if(w == end) {
						bldWrd += '</b>';
					}
					bldWrd += wrd[w];
				}
				return bldWrd;
			}
		
			function findWordsStartingWith(stWrd) {
				var mts = [];
				for(var wrd =0; wrd <= cmvReturns.length-1; wrd++) {
					var wrdMths = 0;
					var stop = true;
					var valid = true;
					for(var x =0; x <= cmvReturns[wrd].length-1; x++) {
						if(cmvReturns[wrd][x] == stWrd[x] && stop) {
							wrdMths +=1;
						} else {
							stop = false;
							if(stWrd.length > wrdMths)
								valid = false;
						}
					}
					if(wrdMths > 0 && valid )  
						mts.push( {word: cmvReturns[wrd], wordsMatched: wrdMths} );
				}
				return mts;
			}
		
			function invertString(str) {
				var iStr = '';
				for(var v = str.length-1; v >=0; v--) {
					iStr += str[v];
				}
				return iStr;
			}
	
		};
		
		eventbus.subscribe("app", "codecompletor", null, true, function(event, data) {
			( new CodeCompletor( data[0], data[1] ) );
		});
		
    }
);
