/**
 * Created by Aaron on 1/2/2016.
 *
 *
 * click -> get_line_clicked --> insert( Range(), atPos)
 *                           \
 *                            |-> delete( Range(), atPos)
 *                            |-> move ( Range(), atPos)
 */

define([ 'app/eventbus', 'app/util', 'app/caretext', 'app/selection', 'app/selectionhandler'],
    function (eventbus, util, caretext, selection, selectionhandler) {

        function EventHandlingProxy() {

            var getOnFocusHandler = function(me, args) {
                me.style['outline'] = 'none';
                //currentLine = parseInt(me.id.substring(14,15));
                me.style['background-color'] = 'rgb(56, 56, 56)';
            };

            var getOnBlurHandler = function(me, args) {
                me.style['background-color'] = '#1d1d1d';
            };

            var getOnClickHandler = function(me, args) {
                var lineSelected = eventbus.publish("internalSharedVars", "currentLine", null, null).get();

                //focus it
                focusThis(lineSelected);

                lineSelected.moveCursorToMousePosition();

            };

            var getOnKeyUpHandler = function(me, args) {
                var keycode = (args.keyCode ? args.keyCode : args.which);

                //do not allow up/down arrow events to run codeanalysis..
                if(keycode != 40 && keycode != 38) {
                    //var cc = new CodeAnalyzer(me);
                    eventbus.publish("app", 'codeanalyzer', null, me );
                    //let only [a-z][A-Z] to make code completion *NEEDS UPDATING
                    if(keycode >= 65 && keycode <= 90) {
                        //cc.runCodeCompletion(args, me);
                        eventbus.publish("app", 'codecompletor', null, [args, me] );
                    }
                }
            };

            var getOnMouseUpHandler = function(me, args) {
                event.preventDefault();
                eventbus.publish("internalSharedVars", "startedDragging", null, null).set([false]);
            };

            var getOnMouseMoveHandler = function(me, args) {
                event.preventDefault();
                var dragging = eventbus.publish("internalSharedVars", "startedDragging", null, null).get();

                if( dragging[0] ) {
                    var currentLine = eventbus.publish("internalSharedVars", "currentLine", null, null).get();
                    currentLine.moveCursorToMousePosition();

                    var startPos = dragging[1];
                    var startElementIndex = dragging[2].getIndex();

                    var endPos = currentLine.getMousePosition();
                    var endElementIndex = currentLine.getIndex();

                    var txtSelectionSection = eventbus.publish("internalSharedVars", "textSelectionSection", null, null).get();
                    txtSelectionSection.innerHTML = "";

                    var newSelection = new selection(
                        {
                            "startIndex": startElementIndex,
                            "endIndex": endElementIndex,
                            "startPos": dragging[1],
                            "endPos": endPos
                        }
                    );

                    var sh = new selectionhandler();
                    sh.insert( newSelection );

                    eventbus.publish("internalSharedVars", "isTextSelected", null, null).set(
                        {
                            "condition": true,
                            "data":  newSelection
                        }
                    );

                }

            };

            var getOnMouseDownHandler = function(me, args) {
                event.preventDefault();
                var currentLine = eventbus.publish("internalSharedVars", "currentLine", null, null).get();

                currentLine.moveCursorToMousePosition(); //bring cursor to current pos

                //clear selection
                var startedDragging = eventbus.publish("internalSharedVars", "startedDragging", null, null).get();

                if( !startedDragging[0] ) {
                    var res = eventbus.publish("internalSharedVars", "isTextSelected", null, null).set( { "condition": false} );
                    var txtSelectionSection = eventbus.publish("internalSharedVars", "textSelectionSection", null, null).get();
                    txtSelectionSection.innerHTML = "";
                }

                var startPosition = currentLine.getMousePosition(); //use the position
                eventbus.publish("internalSharedVars", "startedDragging", null, null).set( [true, startPosition, currentLine]);
            };

            var getOnContentChangeHandler = function(me, args) {
                eventbus.publish("app", 'codeanalyzer', null, me );
            };

            var isKeyImplemented = function( keycode ) {
                var implemented = [13, 8, 9, 38, 40, 37, 39, 46];

                for(var i = 0; i<=implemented.length-1; i++) {
                    if( keycode == implemented[i] ) {
                        return true;
                    }
                }

                return false;
            };

            var focusThis = function(elem) {

                //this can't scale; NEEDS TO BE REWORKED
                //broadcast blur to all other container elements
                var textRenderContainer = eventbus.publish("internalSharedVars", "textRenderingSection", null, null).get();
                var evObj = document.createEvent('UIEvents');
                evObj.initEvent( 'blur', true, true );

                for(var e=0; e<=textRenderContainer.children.length-1;e++) {
                    textRenderContainer.children[e].dispatchEvent(evObj);
                }
                //this is here to force a focus event cause
                var evObj = document.createEvent('UIEvents');
                evObj.initEvent( 'focus', true, true );

                elem.dispatchEvent(evObj);
            };

            var doEnter = function(me) {

                if(util.isCompletorRunning()) {
                    var childs = $('.dropdownmenu').children();
                    for(var c=0; c<=childs.length-1;c++) {
                        if(childs[c]['active'] == 'true') {
                            childs[c].dispatchEvent(new Event('click'));
                        }
                    }

                }else {
                    var currentLine = eventbus.publish("internalSharedVars", "currentLine", null, null).get();
                    var lineText = currentLine.innerText;
                    var caretPos = currentLine.getMousePosition( event.pageX );

                    //preparation to find index and make new line                    
                    var liner = $('.liner');
                    var index = liner.index( currentLine );
                    var textRenderer =  eventbus.publish("internalSharedVars", "textRenderingSection", null, null).get();
                    //create new line
                    var newLine =  eventbus.publish("app", 'linefactory', null, [ this, textRenderer, index+1, null] ); //makeLine(index); //createLineRenderer($("#text-render").get(0).childElementCount);
               
                    //insert it
                    $(newLine).insertAfter(currentLine);

                    //focus it
                    focusThis(newLine);

                    //remove text
                    currentLine.delete( [caretPos, lineText.length] );
                    eventbus.publish("app", 'codeanalyzer', null, currentLine );

                    //add text
                    var endPart = lineText.substring(caretPos, lineText.length);
                    newLine.insert( [0, 0],  endPart);
                    eventbus.publish("app", 'codeanalyzer', null, newLine );

                    eventbus.publish("internalSharedVars", "currentLine", null, null).set( newLine );

                    return false;
                }

            };
            var doBackSpace = function(me) {
                var currentLine = eventbus.publish("internalSharedVars", "currentLine", null, null).get();
                var jLine = $(currentLine);

                //NEEDS MORE WORK
                if(jLine.is(':empty') && jLine.index() != 0)
                {
                    //focus next line
                    var prevLine = jLine.prev()[0];
                    focusThis( prevLine );

                    prevLine.moveCursorToWordPosition( prevLine.innerText.length );

                    jLine.remove();
                    return false;
                }

                var currentLine = eventbus.publish("internalSharedVars", "currentLine", null, null).get();

                var lineText = currentLine.innerText;
                var parsedLine = currentLine.parseMe();
                var currentPos = currentLine.getMousePosition()-1;

                if( currentPos < 0) {
                    return;
                }

                currentLine.delete( [currentPos, currentPos] );

            };

            var doDelete = function(me) {
                var line = eventbus.publish("internalSharedVars", "currentLine", null, null).get();
                var lineText = line.innerText;

                var caretPos = line.getMousePosition( event.pageX );

                var isTextSelected = eventbus.publish("internalSharedVars", "isTextSelected", null, null).get();

                if( isTextSelected["condition"] ) {

                    var currentLine = eventbus.publish("internalSharedVars", "currentLine", null, null).get();
                    currentLine.moveCursorToMousePosition();

                    //var dragging = eventbus.publish("internalSharedVars", "startedDragging", null, null).get();
                    var startPos = getLineByIndex( isTextSelected.data.lineArray[0].startPos );
                    var startElementIndex = isTextSelected.data.lineArray[0].lineIndex;

                    var endPos = currentLine.getMousePosition();
                    var endElementIndex = currentLine.getIndex();

                    var txtSelectionSection = eventbus.publish("internalSharedVars", "textSelectionSection", null, null).get();
                    txtSelectionSection.innerHTML = "";

                    var newSelection = new selection(
                        {
                            "startIndex": startElementIndex,
                            "endIndex": endElementIndex,
                            "startPos": isTextSelected.data.lineArray[0].startPos,
                            "endPos": endPos
                        }
                    );

                    var sh = new selectionhandler();
                    sh.delete( newSelection );

                    eventbus.publish("internalSharedVars", "isTextSelected", null, null).set(
                        {
                            "condition": false,
                            "data":  null
                        }
                    );

                    /*var selectionData = isTextSelected["data"];
                    var startLine = getLineByIndex(selectionData["startIndex"]);
                    var endLine = getLineByIndex(selectionData["endIndex"]);

                    if(selectionData["startIndex"] == selectionData["endIndex"]) {

                        startLine.delete([selectionData["startPos"], selectionData["endPos"]]);

                    } else if(selectionData["startIndex"] < selectionData["endIndex"]) {
                        startLine.delete([selectionData["startPos"], 9999]);

                        var elementIndexDifference = (selectionData["endIndex"] -selectionData["startIndex"]);


                        if( elementIndexDifference >= 1 ) {

                            for(var i =selectionData["startIndex"]+1; i<= selectionData["endIndex"]-1; i++) {

                                var selectionLine = getLineByIndex(i);

                                selectionLine.delete([0, 9999]);

                            }

                        }

                        endLine.delete([0, selectionData["endPos"]]);
                    } else {

                        startLine.delete([0, selectionData["startPos"]]);

                        var elementIndexDifference = (selectionData["startIndex"] - selectionData["endIndex"] );


                        if( elementIndexDifference >= 1 ) {

                            for(var i =selectionData["startIndex"]+1; i<= selectionData["endIndex"]-1; i++) {

                                var selectionLine = getLineByIndex(i);

                                selectionLine.delete([0, 9999]);

                            }

                        }

                        endLine.delete( [selectionData["endPos"], 9999 ]);
                    }
                    */

                } else {
                    line.delete( [caretPos, caretPos] );
                }

                eventbus.publish("app", 'codeanalyzer', null, line );

            };

            var doTab = function(me) {

                var line = eventbus.publish("internalSharedVars", "currentLine", null, null).get();
                var lineText = line.innerText;

                var caretPos = line.getMousePosition( event.pageX );
                var keyCodeChar = '&nbsp;';

                //crude ass tab
                //future version - will check for nearest column and insert the amount of spaces to go there
                for(var i=0; i < 3; i++) {
                    line.insert([caretPos, caretPos], keyCodeChar);
                }

                return false;
            };
            var doUp = function(me) {
                var currentLine = eventbus.publish("internalSharedVars", "currentLine", null, null).get();

                if(util.isCompletorRunning()) {
                    var childs = $('.dropdownmenu').children();

                    for(var c=0; c<=childs.length-1;c++) {
                        if(childs[c]['active'] == 'true')
                        {
                            //Is there another element above?
                            if(c==0){
                                //if there is no upward row, remove code completion
                                util.removeCodeCompletor();
                                return;
                            }

                            //Found active element
                            //Set the current clause as false
                            childs[c]['active'] = 'false';
                            childs[c].style['background-color'] = '#FFF';
                            //Set the new row as true;
                            childs[c-1]['active'] = 'true';
                            childs[c-1].style['background-color'] = 'rgba(201, 201, 201, 0.28)';
                            return;
                        }
                    }

                } else {
                    if($(currentLine).index() != 0)
                    {
                        var line = eventbus.publish("internalSharedVars", "currentLine", null, null).get();
                        var wordPosition = line.getMousePosition();

                        var liner = $('.liner');
                        var index = line.getIndex();
                        var nextLine = liner[index-1];

                        nextLine.moveCursorToWordPosition( wordPosition );

                        //focus next line
                        focusThis( nextLine );
                    }
                }
            };
            var doDown = function(me) {
                if(util.isCompletorRunning()) {
                    var childs = $('.dropdownmenu').children();

                    for(var c=0; c<=childs.length-1;c++) {
                        if(childs[c]['active'] == 'true')
                        {

                            //Is there another element below?
                            if( (c+1)==childs.length ){
                                //if there is no upward row, remove code completion
                                util.removeCodeCompletor();
                                return;
                            }

                            //Found active element
                            //Set the current clause as false
                            childs[c]['active'] = 'false';
                            childs[c].style['background-color'] = '#FFF';
                            //Set the new row as true;
                            childs[c+1]['active'] = 'true';
                            childs[c+1].style['background-color'] = 'rgba(201, 201, 201, 0.28)';
                            return;
                        }
                    }

                } else {

                    var line = eventbus.publish("internalSharedVars", "currentLine", null, null).get();
                    var wordPosition = line.getMousePosition();

                    var liner = $('.liner');
                    var index = line.getIndex();
                    var nextLine = liner[index+1];

                    nextLine.moveCursorToWordPosition( wordPosition );

                    //focus next line
                    focusThis( nextLine );

                }
            };
            var doLeft = function(me) {
                var currentLine = eventbus.publish("internalSharedVars", "currentLine", null, null).get();

                var parsedLine = currentLine.parseMe();
                var currentPos = currentLine.getMousePosition();

                if( currentPos == 0) {
                    return;
                }

                currentLine.moveCursorToWordPosition(currentPos-2);
            };
            var doRight = function(me) {

                var currentLine = eventbus.publish("internalSharedVars", "currentLine", null, null).get();

                var parsedLine = currentLine.parseMe();
                var currentPos = currentLine.getMousePosition();

                if( currentPos == parsedLine.length) {
                    return;
                }

                currentLine.moveCursorToWordPosition(currentPos);

            };

            var getOnKeyDownHandler = function(me, args) {
                var keycode = (args.keyCode ? args.keyCode : args.which);
                newLineRend = me;

                if( isKeyImplemented(keycode) ) {

                    args.preventDefault();

                    switch (keycode) {
                        case 8:
                            doBackSpace(me);
                            break;
                        case 9:
                            doTab(me);
                            break;
                        case 13:
                            doEnter(me);
                            break;
                        case 37:
                            doLeft(me);
                            break;
                        case 38:
                            doUp(me);
                            break;
                        case 39:
                            doRight(me);
                            break;
                        case 40:
                            doDown(me);
                            break;
                        case 46:
                            doDelete(me);
                            break;
                        default:
                            //what happened heir?
                            console.log("Something bad happened here!")
                    };

                }
                else if( keycode == 32 ||
                    (keycode >= 48 && keycode <= 57) ||
                    (keycode >= 65 && keycode <= 90) ||
                    keycode == 188 ||
                    keycode == 190 ||
                    keycode == 191 ||
                    keycode == 192 ||
                    keycode == 219 ||
                    keycode == 220 ||
                    keycode == 221 ||
                    keycode == 222 ) // alphanumeric chars; numbers and letters
                {
                    args.preventDefault();

                    var line = eventbus.publish("internalSharedVars", "currentLine", null, null).get();
                    var lineText = line.innerText;

                    var caretPos = line.getMousePosition( event.pageX );
                    var keyCodeChar;

                    if( keycode == 32 ) {
                        keyCodeChar = '&nbsp;';
                    } else {
                        if( ! args.shiftKey ) {
                            keyCodeChar = String.fromCharCode(keycode).toLowerCase();
                        } else {
                            keyCodeChar = String.fromCharCode(keycode);
                        }
                    }

                    line.insert( [caretPos, caretPos], keyCodeChar );

                }
                else if(newLineRend.offsetHeight < newLineRend.scrollHeight)
                {
                    return false;
                }

                var isTextSelected = eventbus.publish("internalSharedVars", "isTextSelected", null, null).get();

                if( isTextSelected["condition"] ) {
                    var res = eventbus.publish("internalSharedVars", "isTextSelected", null, null).set( { "condition": false} );
                    var txtSelectionSection = eventbus.publish("internalSharedVars", "textSelectionSection", null, null).get();
                    txtSelectionSection.innerHTML = "";
                }

                eventbus.publish("app", 'codeanalyzer', null, me );

            };

            Array.prototype.insert = function(index) {
                this.splice.apply(this, [index, 0].concat(
                    Array.prototype.slice.call(arguments, 1)));
                return this;
            };

            var getLineByIndex = function( index ) {
                return $('.liner')[index];
            };

            // CONSTRUCTOR HERE

            //register all events needed
            var txtRenderingLayer = $('.textRenderingSection');
            var canvas = $('.ideCanvas');

            eventbus.subscribe("elem", "click", canvas[0], true, getOnClickHandler);

            txtRenderingLayer.on('DOMNodeInserted', function(e) {

                var newLineRend = e.target;

                if ( e.target.nodeName == 'DIV' && e.target.className == 'liner') {
                    eventbus.subscribe("elem", "focus", newLineRend, true, getOnFocusHandler);
                    eventbus.subscribe("elem", "blur", newLineRend, true, getOnBlurHandler);
                }
            });

            eventbus.subscribe("elem", "mousedown", canvas[0], true, getOnMouseDownHandler);
            eventbus.subscribe("elem", "mousemove", canvas[0], true, getOnMouseMoveHandler);
            eventbus.subscribe("elem", "mouseup", canvas[0], true, getOnMouseUpHandler);

            eventbus.subscribe("elem", "contentchange", canvas[0], true, getOnContentChangeHandler);

            //this needs to be better (HERE FOR TESTING PURPOSES)
            eventbus.subscribe("elem", "keydown", (new caretext()).getInstance()[0], true, getOnKeyDownHandler);

            eventbus.subscribeVar('startedDragging', false);
            eventbus.subscribeVar('isTextSelected', false);
        };

        return EventHandlingProxy;
    }
);

