/**
 * Created by Aaron on 2/24/2016.
 */
define(['app/eventbus'],
    function ( eventbus ) {

        var CaretHandler = function() {
            //must be made better
            var tx = $('textarea.inputTX');

            //might need refactoring // maybe a new cursor class?
            this.move = function( X, Y ) {

                //move the textarea
                tx.focus();

                tx.css('left', X + 'px');
                tx.css('top', Y + 'px');
            };

            this.moveX = function(elem, X) {
                tx.css('left', X + 'px');
                var index = elem.getIndex();
                this.moveY( elem.getClickPosition().height * index+1 );
            };

            this.moveY = function(Y) {
                tx.css('top', Y + 'px');
            };

            this.getInstance = function() {
                return tx;
            }

        };

        return CaretHandler;

    }
);

