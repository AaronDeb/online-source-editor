
requirejs.config({
    //By default load any module IDs from js/lib
    baseUrl: 'js/lib',
    //except, if the module ID starts with "app",
    //load it from the js/app directory. paths
    //config is relative to the baseUrl, and
    //never includes a ".js" extension since
    //the paths config could be for a directory.
    paths: {
        app: '../app',
        jquery: 'https://code.jquery.com/jquery-1.10.2.min',
        underscore: 'https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.3.3/underscore-min'
    },
    
    shim: {
        "underscore": {
            exports: "_"
        }
    }
    
});

// Start the main app logic.
requirejs( ['jquery', 'app/editor'],
    function($, editor) { 
        //jQuery, canvas and the app/sub module are all
        //loaded and can be used here now.
        

        $(document).ready( function() {
        
        	var data = [ 'public static string init()', '{', '}' ];
        
        	editor.makeWithData( '.ide-section', data );
        
        });
        

    }
);