# online-source-editor

This is a library that allows a developer to setup a functional C# ide like section in-browser for either showcasing/code editing reasons. 

Current status
- Master branch is the development branch which is unfinished and highly unstable. 

Current version
- 0.5

Current features
- Simple C# Syntax highlighter in place
- Most input keys implemented
- Simple Code Completor

Demo

![alt tag](https://raw.githubusercontent.com/AaronDeb/online-source-editor/master/demo.gif)